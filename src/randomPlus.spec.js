const randomPlus = require("./randomPlus");
const random = require("./random");

jest.mock("./random");
random.mockReturnValue(100);

describe("random", () => {
	it("number is 105 if random return 100 and randomPlus argument 5", () => {
		random.mockReturnValueOnce(100);
		const rnd = randomPlus(5);
		expect(rnd).toBe(105);
	});
	it("random arguments from randomPlus is 1 and 99", () => {
		randomPlus(5);
		expect(random).lastCalledWith(1, 99);
	});
});
