const user = require("./user");

describe("user", () => {
	it("user 1 to match snapshot", () => {
		const usr = user(1);
		expect(usr).toMatchSnapshot();
	});
	it("user 56 to match snapshot", () => {
		const usr = user(56);
		expect(usr).toMatchSnapshot();
	});
	it("user 1345 to match snapshot", () => {
		const usr = user(1345);
		expect(usr).toMatchSnapshot();
	});
	it("user id as string gives exception", () => {
		expect(() => {
			user("test");
		}).toThrowError();
	});
});
